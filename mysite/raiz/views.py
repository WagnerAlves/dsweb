from django.shortcuts import render
from django.views import View

# Create your views here.

class IndexView(View):
    def get(self, request, *args, **kwargs):
        return render(request, "raiz/index.html")


# def index(request):
#     return render(request, 'raiz/index.html')