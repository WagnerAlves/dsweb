from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .models import Pergunta, Alternativa
from django.urls import reverse
from django.views import View

# Create your views here


class IndexView(View):
    def get(self, request, *args, **kwargs):
        enquetes = Pergunta.objects.order_by('-data_pub')[:10]
        contexto = {'lista_enquetes': enquetes}
        return render(request, 'enquetes/index.html', contexto)


class DetalhesView(View):
    template = 'enquetes/pergunta_detail.html'

    def resposta(self, request, pergunta, error):
        if error:
            contexto = {'enquete': pergunta, 'error': error}
        else:
            contexto = {'enquete': pergunta}
        return render(request, self.template, contexto)

    def get(self, request, *args, **kwargs):
        pergunta_id = kwargs['pk']
        pergunta = get_object_or_404(Pergunta, pk = pergunta_id)
        return self.resposta(request, pergunta, None)

    def post(self, request, *args, **kwargs):
        pergunta_id = kwargs['pk']
        pergunta = get_object_or_404(Pergunta, pk=pergunta_id)
        try:
            id_alternativa = request.POST['escolha']
            alt = pergunta.alternativa_set.get(pk=id_alternativa)
        except (KeyError, Alternativa.DoesNotExist):
            error = 'Você precisa selecionar uma alternativa.'
            return self.resposta(request, pergunta, error)
        else:
            alt.quant_votos += 1
            alt.save()
            return HttpResponseRedirect(reverse('enquetes:resultado', args=(pergunta.id,)))


class ResultadoView(View):
    def get(self, request, *args, **kwargs):
        pergunta_id = kwargs['pk']
        pergunta = get_object_or_404(Pergunta, pk = pergunta_id)
        contexto = {'enquete': pergunta}
        return render(request, 'enquetes/resultado.html', contexto)


"""
Poderia ter feito assim, bem mais fácil...
from django.shortcuts import get_object_or_404, render
def detalhes(request, pergunta_id):
    questao = get_object_or_404(Pergunta, pk= pergunta_id)
    return render(request, 'enquetes/detalhes.html', {'questao': questao})


def index(request):
    lista = Pergunta.objects.all()
    template = loader.get_template('enquetes/index.html')
    contexto = {'lista_enquetes': lista}
    return HttpResponse(template.render(contexto, request))


def resultado(request, pergunta_id):
    pergunta = get_object_or_404(Pergunta, pk= pergunta_id)
    return render(request, 'enquetes/resultado.html', {'enquete': pergunta})#esse resto aqui é o contexto


def detalhes(request, pergunta_id):
    try:
         pergunta = Pergunta.objects.get(pk = pergunta_id)
    except Pergunta.DoesNotExist:
         raise Http404('Identificação de enquete inválida!')
    return render(request, 'enquetes/detalhes.html', {'enquete': pergunta})


def votacao(request, pergunta_id):
    pergunta = get_object_or_404(Pergunta, pk=pergunta_id)
    try:
        id_alternativa = request.POST['escolha']
        alt = pergunta.alternativa_set.get(pk=id_alternativa)
    except (KeyError, Alternativa.DoesNotExist):
        contexto = {
            'enquete': pergunta,
            'error': 'Você precisa selecionar uma alternativa.'
        }
        return render(request, 'enquetes/detalhes.html', contexto)
    else:
        alt.quant_votos += 1
        alt.save()
        return HttpResponseRedirect(reverse('enquetes:resultado', args=(pergunta.id,)))
"""